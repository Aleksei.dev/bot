'use strict';
document.addEventListener('DOMContentLoaded', () => {
  const getRandomIntInclusive = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  const botWrap = () => {
    const number = getRandomIntInclusive(1, 10);
    console.log(number);
    let count = 10;
    const bot = () => {
      const answer = +prompt('Угадай число от 1 до 10', '');
      console.log(answer);
      if (answer === 0) {
        console.log('Игра окончена');
      } else if (count === 0) {
        const newGame = confirm('Попытки закончились, хотите сыграть еще?');
        if (newGame) {
          botWrap();
        }
      } else if (answer > number) {
        console.log(`Загаданное число меньше, осталось попыток ${--count}`);
        console.log(count);
        bot();
      } else if (answer < number) {
        console.log(`Загаданное число больше, осталось попыток ${--count}`);
        console.log(count);
        bot();
      } else if (isNaN(answer)) {
        console.log('Введи число!');
        bot();
      } else if (answer === number) {
        const newGame = confirm('Вы угадали!!! Хотели бы сыграть еще?');
        if (newGame) {
          botWrap();
        }
      }
    };
    bot();
  };
  botWrap();
});
